from compartment.updateOxygenSteady import *
from compartment.updateVASteady import *
from compartment.updateSubCellular import *
from compartment.updateVessel import *
from compartment.updateCell import *
from compartment.updateChemo import updateChemo
from compartment.const import (cell,
                   grid,
                   avastin,
                   drug,
                   oxygen,
                   vegf,
                   vessel)
import compartment.initialiseVessel as initialiseVessel
from compartment.utilFunc import *
import compartment.automata as automata

from dolfin import *
import argparse
import math
import time
import numpy as np
import os
import errno
import json
import gzip

def main(cell_filename, grid_size, vp, ktrans, ktransNew, vessel_sep, Tcell, TcellSD, chemo_theta, schedule, dose, vegf_low = 1e-6, vegf_high = 1e-4, prob_birth=0.002, prob_death=0.0001, hypoxia_scale1 = 1, hypoxia_scale2=1, hypoxia_scale3=1,nWeek = 12, dt_update = 30, **options):
    grid_ny, grid_nx = grid_size
    print(grid_ny)
    print(grid_nx)
    #Read in the initialisation of cell distribution
    try:
        cell_file = open(cell_filename)
        cell_init = [line.strip() for line in cell_file]
        cell_file.close()
        cell_init = np.array(cell_init, dtype = float)
    except (OSError, IOError): #Generate random int matrix if no cell config found
        print('Cell configuration file not found! Will generate a random one.\n')
        n_grid_points = (grid_ny+1)*(grid_nx+1)
        frac_normal = float(raw_input('Enter normal cell fraction = '))
        frac_cancer = float(raw_input('Enter cancer cell fraction = '))
        n_normal_cells = int(round(n_grid_points*frac_normal))
        n_cells = int(round(n_normal_cells + n_grid_points*frac_cancer))
        rand_non_repeat = np.random.choice(n_grid_points, n_cells, replace=False)
        cell_init = np.zeros(n_grid_points)
        for i in rand_non_repeat[0:n_normal_cells]:
            cell_init[i] = 1
        for i in rand_non_repeat[n_normal_cells:]:
            cell_init[i] = 2

    # Load FEniCS functionspace for the size
    grid.loadFEM(grid_ny, grid_nx)

    # Set initial condition
    permVessel = np.zeros((grid_nx + 1) * (grid_ny + 1)) #define permenent vessels
    vessel_init = np.zeros((grid_nx + 1) * (grid_ny + 1))
    print('test what is grid_nx {0:3d}, grid_ny {1:3d}\n'.format(grid_nx,grid_ny))
    while True: # raise error and reduce sep b/w vessels if too many vessels
        try:
            vIndex = initialiseVessel.vesselIndex(grid_ny + 1, grid_nx + 1, vessel.count(grid_nx, grid_ny, vp), vessel_sep, True) #initialise vessels
        except IndexError:
            vessel_sep -= 1
            print('Not enough space to place vessels, separation between vessels reduced to {0:2d}.'.format(vessel_sep)) 
            continue
        break
        
    for i in vIndex:
        vessel_init[i] = 1.0
    grid.myVessel.vector()[:] = vessel_init[grid.permSetValue] # initialise mesh value according to DOF map
    o2Init= np.zeros((grid_ny+1)*(grid_nx+1)) #initialise oxygen
    grid.K.vector()[:] = o2Init
#    print(sum(cell_init ==1))

    ## Initialise cell with two types, cell.Type = 2 if cancer cells, cell.Type = 1 otherwise (inc. stroma and normal cells)

    cellPop = automata.Automata(grid_nx + 1, grid_ny + 1, (cell_init != 0)*1, o2Init,Tcell,TcellSD,(cell_init == 1)*1) #initialise array of cell object
    cell_loc = np.array([x.getState() == 1 for i in cellPop.cells for x in i], dtype = float) # get cell location
    cellWrite = np.array([x.getType() for i in cellPop.cells for x in i], dtype = float)
    temp = np.array([0 if np.isnan(x) else x for x in cellWrite])
    grid.myCell.vector()[:] = cell_loc[grid.permSetValue]
    grid.myCellType.vector()[:] = temp[grid.permSetValue] 
    tTotal = int(nWeek * 7 * 24 * 60) #total simulation time in minutes
    
    #Directory for saving files
    if options['write_to_file']:
        result_padding = 1
        result_existed = False
        if options['path'] is None: #no directory specified
            result_dir = './' + cell_filename + '-' + str(ktrans) + '-' + str(vp) + '-' + time.strftime("%Y%m%d-%H%M%S")
        else:
            result_dir = options['path']
        while not result_existed: #check existence of directory
            try:
                os.makedirs(result_dir)
                result_existed = True
            except OSError as exception:
                if exception.errno != errno.EEXIST:
                    raise
                result_dir = result_dir + str(result_padding)
                result_padding +=1
            
        o2File = File(result_dir + '/' + 'o2File.pvd')
        vegfFile = File(result_dir + '/' + 'vegfFile.pvd')
        avastinFile = File(result_dir + '/' + 'avastinFile.pvd')
        cellFile = File(result_dir + '/' + 'cellFile.pvd')
        vesselFile = File(result_dir + '/' + 'vesselFile.pvd')
        G1File = File(result_dir + '/' + 'G1File.pvd')
        G2File = File(result_dir + '/' + 'G2File.pvd')
        G3File = File(result_dir + '/' + 'G3File.pvd')

    else: #return in numpy array
        o2_save = np.zeros(shape=(int(tTotal/dt_update),len(cell_loc)))
        vegf_save = np.zeros(shape=(int(tTotal/dt_update),len(cell_loc)))
        avastin_save = np.zeros(shape=(int(tTotal/dt_update),len(cell_loc)))
        cell_save = np.zeros(shape=(int(tTotal/dt_update),len(cell_loc)))
        vessel_save = np.zeros(shape=(int(tTotal/dt_update),len(cell_loc)))
        G1_save = np.zeros(shape=(int(tTotal/dt_update),len(cell_loc)))
        G2_save = np.zeros(shape=(int(tTotal/dt_update),len(cell_loc)))
        G3_save = np.zeros(shape=(int(tTotal/dt_update),len(cell_loc)))
        cellcycle_save = np.zeros(shape=(int(tTotal/dt_update),len(cell_loc)))          

    ## Initialise drug plasma concentration accordingly to dose and schedule
    drug_plasma = {}

    drug_plasma['G1'] = drug.CpG1mod(dt_drug=schedule['G1'], dose=dose['G1'], dt_update = 0.1, t_total=nWeek)
    drug_plasma['G2'] = drug.CpG2(dt_drug=schedule['G2'], dose=dose['G2'], dt_update = 0.1, t_total=nWeek)
    drug_plasma['G3'] = drug.CpG3(dt_drug=schedule['G3'], dose=dose['G3'], dt_update=0.1, t_total=nWeek)
    drug_plasma['avastin'] = avastin.Cp(dt_drug = schedule['avastin'],dose=dose['avastin'], dt_update=0.05, t_total=nWeek)
    dt_chemo = 1
    drug_plasma['G1'] = drug_plasma['G1'][0::int(dt_chemo/0.1)]
    drug_plasma['G2'] = drug_plasma['G2'][0::int(dt_chemo/0.1)]
    drug_plasma['G3'] = drug_plasma['G3'][0::int(dt_chemo/0.1)]

    
#    drug_plasma_peak = {key: max(value) for key, value in drug_plasma.items()}

    ## Initialise oxygen and subcellular module from steady state
    
    updateOxygenSteady(grid.myVessel, grid.myVesselNew, grid.myCell, grid.K, grid.v, grid.g)

    for ti in range(0, 3 * 24 * 60, dt_update):
        updateSubCellular(dt_update, hypoxia_scale1, hypoxia_scale2, hypoxia_scale3, grid.K, grid.TP53, grid.VEGFs)        
        
    cellSum0 = grid.myCell.vector().sum()
    vesselSum0 = grid.myVessel.vector().sum() + \
        grid.myVesselNew.vector().sum()
    (V, A_e, C) = grid.VA.split(deepcopy = True)

    cellWrite = np.array([x.getType() for i in cellPop.cells for x in i], dtype = float)
    temp = np.array([0 if np.isnan(x) else x for x in cellWrite])
    grid.myCellType.vector()[:] = temp[grid.permSetValue]
    if options['write_to_file']:
        vegfFile << V
        avastinFile << A_e
        vesselFile << grid.myVessel
        o2File << grid.K    
        cellFile << grid.myCellType
        G1File << grid.G1
        G2File << grid.G2
        G3File << grid.G3        
    else:
        vegf_save[0] = V.vector()[grid.permGetValue]
        avastin_save[0]= A_e.vector()[grid.permGetValue]
        vessel_save[0] = grid.myVessel.vector()[grid.permGetValue]
        o2_save[0] = grid.K.vector()[grid.permGetValue]
        cell_save[0] = grid.myCellType.vector()[grid.permGetValue]
        G1_save[0]=grid.G1.vector()[grid.permGetValue]
        G2_save[0]=grid.G2.vector()[grid.permGetValue]        
        G3_save[0]=grid.G3.vector()[grid.permGetValue]
        cellcycle_save[0] = np.array([x.cycle for i in cellPop.cells for x in i], dtype = float)
    logf = open("error.log", "w")
    ## update each module until end of simulation or no cancer cell remains
    for ii, ti in enumerate(range(dt_update,tTotal+1, dt_update)):
        #update vessel
        if ti % (60 * nWeek) == 0:
            updateVessel(vegf_low, vegf_high, prob_birth, prob_death, permVessel, V, grid.myVessel, grid.myVesselNew)
        cellSum = grid.myCell.vector().sum()
        vesselSum = grid.myVessel.vector().sum() + grid.myVesselNew.vector().sum()
        #No updates if no vessel left
        try:
            updateVASteady(grid.myVessel, grid.myVesselNew, grid.myCell, grid.VEGFs, drug_plasma['avastin'], 0.05, ti, grid.VA, ktrans, ktransNew, grid.p, grid.q, grid.s)
            updateChemo(grid.myVessel, grid.myVesselNew, grid.myCell, drug_plasma['G1'], dt_chemo, ti, grid.mymesh, grid.G1,ktrans, ktransNew)
            updateChemo(grid.myVessel, grid.myVesselNew, grid.myCell, drug_plasma['G2'], dt_chemo, ti, grid.mymesh, grid.G2,ktrans, ktransNew)
            updateChemo(grid.myVessel, grid.myVesselNew, grid.myCell, drug_plasma['G3'], dt_chemo, ti, grid.mymesh, grid.G3,ktrans, ktransNew)
            (V, A_e, C) = grid.VA.split(True)
            updateSubCellular(dt_update, hypoxia_scale1, hypoxia_scale2, hypoxia_scale3, grid.K, grid.TP53, grid.VEGFs)        
            updateCell(cellPop, grid.myCell, grid.K, grid.G1, grid.G2, grid.G3, dt_update, chemo_theta)
            updateOxygenSteady(grid.myVessel, grid.myVesselNew, grid.myCell, grid.K, grid.v, grid.g)
        except RuntimeError:
            logf.write('Failed to update, will try again at the next iteration.\n')
            pass
        if cellSum != cellSum0:
            cellSum0, vesselSum0 = (cellSum, vesselSum)
            if not options['nice']:
                print('cellSum = {0:3.0f}, vesselSum = {1:3.0f}'.format(cellSum0, vesselSum0))
        cancercellCopy = np.array([x.getType() == 2 for i in cellPop.cells for x in i], dtype = float)
        (V, A_e, C) = grid.VA.split(deepcopy = True)        
        cellWrite = np.array([x.getType() for i in cellPop.cells for x in i], dtype = float)
        temp = np.array([0 if np.isnan(x) else x for x in cellWrite])
        grid.myCellType.vector()[:] = temp[grid.permSetValue]
        if options['write_to_file']:        
            vegfFile << V
            avastinFile << A_e
            vesselFile << grid.myVessel
            o2File << grid.K
            cellFile << grid.myCellType
            G1File << grid.G1
            G2File << grid.G2            
            G3File << grid.G3
            
        else:
            vegf_save[ii+1] = V.vector()[grid.permGetValue]
            avastin_save[ii+1]= A_e.vector()[grid.permGetValue]
            vessel_save[ii+1] = grid.myVessel.vector()[grid.permGetValue] + grid.myVesselNew.vector()[grid.permGetValue]
            o2_save[ii+1] = grid.K.vector()[grid.permGetValue]
            cell_save[ii+1] = grid.myCellType.vector()[grid.permGetValue]
            G1_save[ii+1]=grid.G1.vector()[grid.permGetValue]
            G2_save[ii+1]=grid.G2.vector()[grid.permGetValue]
            G3_save[ii+1]=grid.G3.vector()[grid.permGetValue]
            cellcycle_save[ii+1] = np.array([x.cycle for i in cellPop.cells for x in i], dtype = float)            
        if sum(cancercellCopy) == 0:
            print('All cancer cells killed! Terminating')
            break
    logf.close()
    try:
        return (vegf_save, avastin_save, vessel_save, o2_save, cell_save, G1_save, G2_save, G3_save, cellcycle_save)
    except NameError:
        pass
 
    
if __name__ == '__main__':
   parser = argparse.ArgumentParser(description='Run simulation of therapy for patients.')
   parser.add_argument('cell_filename', help = 'File name for cell initialisation')
   parser.add_argument('size', nargs='+', help = 'Define size of the simulated grid nx, ny', type = int)
   parser.add_argument('t_cell', help = 'Cell cycle length in days', type = float)
   parser.add_argument('--tCellSD', help = 'SD of cell cycle length', default=1.0, type=float)
   parser.add_argument('-v','--vp', default = 11.97, type = float)
   parser.add_argument('-k','--ktrans', default = 0.14, type = float)
   parser.add_argument('--ktransNew', default = 0.14, type = float)   
   parser.add_argument('-s','--vessel_sep', default = 4, type = int)
   parser.add_argument('-p','--p_birth', default = 0.002, help = 'Probability of creating a vessel', type = float)
   parser.add_argument('-q','--p_death', default = 0.0001,help = 'Probability of deleting a vessel', type = float)
   parser.add_argument('-n','--nWeek', default = 12, help = 'Length of simultion in weeks, default to whole treatment of 12 weeks', type = float)
   parser.add_argument('-d','--dt_update', default = 30, help = 'Length of update interval in minutes, default to 30 mins', type = int)
   parser.add_argument('-a','--vegf_low', default = 1e-6, help = 'Lower threshold for stable vegf level', type = float)
   parser.add_argument('-b','--vegf_high', default = 1e-4, help = 'Higher threshold for stable vegf level', type = float)
   parser.add_argument('-x','--hypoxia_scale',default = 1, help = 'Scaling parameter for hypoxic level', type = float)
   parser.add_argument('--hypoxia_scale2',default = 1, help = 'Scaling parameter 2 for hypoxic level', type = float)
   parser.add_argument('--hypoxia_scale3',default = 1, help = 'Scaling parameter 3 for hypoxic level', type = float)
   parser.add_argument('-z','--chemo_sensitivity', help = "Sensitivity parameter for chemotherapy. Input should be string of dictionary. For example \'\{\"G1\"\: 10000,\"G2\"\: 10000\}\' ", type = json.loads)
   parser.add_argument('--schedule', help = "Time inbetween each dose of drugs (in weeks). Input should be string of dictionary. For example \'\{\"G1\"\: 3,\"G2\"\: 3\}\' ", type = json.loads)
   parser.add_argument('--dose', help = "Dosage of each drug in mg. Input should be string of dictionary. For example \'\{\"G1\"\:600,\"G2\"\:100\}\' ", type = json.loads)
   parser.add_argument('--seed', help = "Set global random seed for the simulation", type = int)
   parser.add_argument('-o','--outpath', help = 'Set output path')
   parser.add_argument('--output', dest='wtf', action='store_true')
   parser.add_argument('--no-output', dest='wtf', action='store_false')
   parser.add_argument('--nice', dest='nice', action='store_true')
   parser.set_defaults(wtf=True)

   args = parser.parse_args()
   # set numpy array printing format
   eps = np.finfo(float).eps
   np.set_printoptions(precision=4)
   # set random seed
   np.random.seed(args.seed)

   (vegf_save, avastin_save, vessel_save, o2_save, cell_save, G1_save, G2_save, G3_save, cellcycle_save) = \
   main(args.cell_filename, args.size, args.vp, args.ktrans, args.ktransNew, args.vessel_sep, args.t_cell, args.tCellSD, args.chemo_sensitivity, args.schedule, args.dose, args.vegf_low, args.vegf_high, args.p_birth, args.p_death,  args.hypoxia_scale, args.hypoxia_scale2, args.hypoxia_scale3, args.nWeek, args.dt_update, write_to_file = args.wtf, path = args.outpath, nice = args.nice)

   with gzip.GzipFile(args.outpath + "/" + "vegf.npy.gz", "wb") as f1:
       np.save(f1, vegf_save)
   with gzip.GzipFile(args.outpath + "/" + "avastin.npy.gz", "wb") as f2:
       np.save(f2, avastin_save)
   with gzip.GzipFile(args.outpath + "/" + "vessel.npy.gz", "wb") as f3:
       np.save(f3, vessel_save)
   with gzip.GzipFile(args.outpath + "/" + "o2.npy.gz", "wb") as f4:
       np.save(f4, o2_save)
   with gzip.GzipFile(args.outpath + "/" + "cell.npy.gz", "wb") as f5:
       np.save(f5, cell_save)
   with gzip.GzipFile(args.outpath + "/" + "G1.npy.gz", "wb") as f6:
       np.save(f6, G1_save)
   with gzip.GzipFile(args.outpath + "/" + "G2.npy.gz", "wb") as f7:
       np.save(f7, G2_save)
   with gzip.GzipFile(args.outpath + "/" + "G3.npy.gz", "wb") as f8:
       np.save(f8, G3_save)
   with gzip.GzipFile(args.outpath + "/" + "cellcycle.npy.gz", "wb") as f9:
       np.save(f9, cellcycle_save)
