#import compartment.const.oxygen as o2
import compartment.const.grid as grid
import compartment.const.cell as cell
import compartment.const.drug as drug
import numpy as np
from scipy.stats import beta
import random
import compartment.automata
def updateCell(cellPop, myCell, K, G1, G2, G3, dt_update, theta):
    ''' Update Cell according to cellular automaton rules.
    
    Args:
    cellPop (Array): Automata object storing array of Cell object 
    myCell (Function): 1 = there is a cell, 0 = otherwise
    K (Function): previous solution of oxygen
    G1 (Function): previous solution of chemo G1
    G2 (Function): previous solution of chemo G2
    G3 (Function): previous solution of chemo G3
    dt_update: update interval of the steady-state solution
    theta (Dictionary): chemo sensitivity parameter
    '''
    o2Copy = K.vector()[grid.permGetValue]
    G1Copy = G1.vector()[grid.permGetValue]
    G2Copy = G2.vector()[grid.permGetValue]
    G3Copy = G3.vector()[grid.permGetValue]
    
    G1KillIndex = np.random.binomial(1,beta.cdf(G1Copy/drug.G1PPC,1,theta['G1']))
    G2KillIndex = np.random.binomial(1,beta.cdf(G2Copy/drug.G2PPC,1,theta['G2']))
    G3KillIndex = np.random.binomial(1,beta.cdf(G3Copy/drug.G3PPC,1,theta['G3']))
    cellCopy = np.array([x.getState() == 1 for i in cellPop.cells for x in i], dtype = float)
    
    counter = 0
    for i in cellPop.cells:
        for j in i:
            j.setOxygen(o2Copy[counter])
#            if j.getType() == 1: #Nomal cell death by hypoxia
            if j.getType() == 2: #Cancer cell death by chemotherapy
                currentCycle = j.getCellCycle()                
                currentCycle += o2Copy[counter] / (cell.Kphi + o2Copy[counter]) / cell.Tmin(j.Tcell) * dt_update
                j.setCellCycle(min(currentCycle,1.))
                if (G1KillIndex[counter] or G2KillIndex[counter] or G3KillIndex[counter]) and currentCycle >= 1. :
                    j.reset(o2Copy[counter])
            counter += 1
    cellCopy = np.array([x.getState() == 1 for i in cellPop.cells for x in i], dtype = float)
    cellPop.nextGeneration()
    cellCopy = np.array([x.getState() == 1 for i in cellPop.cells for x in i], dtype = float)
    myCell.vector()[:] = cellCopy[grid.permSetValue]    
    
