import __init__
from dolfin import *
import compartment.const.oxygen as o2

def updateOxygenSteady(source, sourceNew, sink, K, v, k):
    """This is a module update steady-state solution of oxygen.

    Args:
        source (Function): Vessel, source of oxygen
        sourceNew (Function): Newly created vessel, source of oxygen
        sink (Function): Cell, sink of oxygen
        K (Function): previous solution of oxygen
        v (TestFunction): TestFunction of CG1
    """
    # Define variational formulation of oxygen concentration K
    K.vector()[:]=0.
    F = - inner(o2.D*grad(K), grad(v))*dx + o2.r*(o2.K0 - K)*(source + sourceNew)*v*dx - \
        o2.phi*K/(K + o2.K1)*sink*v*dx
    dF = derivative(F, K, k)
    problem = NonlinearVariationalProblem(F, K, None, dF) 
    solver = NonlinearVariationalSolver(problem)
    
    solver.parameters["newton_solver"]["convergence_criterion"] = "incremental"
    solver.parameters["newton_solver"]["maximum_iterations"] = 10
        
    solver.solve()    
#    solve(F == 0, interpolate(, solver_parameters = {'newton_solver': {'convergence_criterion': 'incremental'}})
    
# Demonstration of the oxygen module
if __name__ == "__main__":
    from compartment.const import grid
    from compartment import initialiseVessel
    import numpy as np

    #set random seed
    np.random.seed(1000)

    ny = 23
    nx = 31
    grid.loadFEM(ny,nx)

    # Initialise vessels randomly
    vessel = grid.myVessel
    vp = 10.97
    vessel_init = np.zeros((nx + 1) * (ny + 1))    
    vIndex = initialiseVessel.vesselIndex(ny, nx, int(ny*nx*100*vp/100/3.1415926/(10**2)), 3, True)
    for i in vIndex:
        vessel_init[i] = 1.0
    vessel.vector()[:] = vessel_init[grid.permSetValue] # initialise mes    

    # Initialise grid. randomly
    myCell = grid.myCell
    n_grid_points = (23+1)*(31+1)
    frac_normal = 0.2
    frac_cancer = 0.5
    n_normal_cells = int(round(n_grid_points*frac_normal))
    n_cells = int(round(n_normal_cells + n_grid_points*frac_cancer))
    rand_non_repeat = np.random.choice(n_grid_points, n_cells, replace=False)
    cell_init = np.zeros(n_grid_points)
    for i in rand_non_repeat[0:n_normal_cells]:
        cell_init[i] = 1
    for i in rand_non_repeat[n_normal_cells:]:
            cell_init[i] = 1
    myCell.vector()[:] = cell_init

    ktrans = 0.001
    ktransNew = 0.001
    
    o2File = File('./o2File.pvd') # save results

    K = grid.K
    K.vector()[:] = 18.

    VV = FunctionSpace(grid.mymesh,"CG",1)
    
    timer = Timer("Oxygen PDE solve")
    for i in range(2):
        updateOxygenSteady(vessel, grid.myVesselNew, myCell, K, grid.v, grid.g)
        print(K.vector()[:].min())
        print(K.vector()[:].max())
        
    o2File << K
    toc = timer.stop()
    print("time to solve = {}".format(toc))
