import random
from compartment.utilFunc import *

def vesselIndex(nY, nX, nVessel, nb = 1, ravel = True):
    """ Define a random vessel initialisation in a nY by nX lattice 

    Args:
        nY (int): number of lattice points in y-direction
        nX (int): number of lattice points in x-direction
        nVessel (int): number of allocated vessels
        nb (int): distance between neighbouring vessels
        ravel (boo): return linear indices if True, and coordinates otherwise
    Returns:
        list: linear indices/coordinates of vessels 
    """
    vCoor = []
    # draw first vessel
#    (y0,x0) = random.randrange(nY), random.randrange(nX) # draw first vessel
    (y0,x0) = int(round((nY-1)/2)), int(round((nX-1)/2))
    vCoor.append((y0,x0))
    nb_candidate = neighboursBorder(nY,nX,y0,x0,nb) #list of candidate vessel locations

    # draw other vessels
    for i in range(1,nVessel):
        new = random.choice(nb_candidate) # choose one random loc
        vCoor.append(new)
        nb_candidate.remove(new) # remove from candidate list
        temp = neighboursBorder(nY,nX,new[0],new[1],nb) # draw a new list of candidates from the new location
        temp1 = [x for x in temp + nb_candidate if min([distSquared(x,y) for y in vCoor]) >= nb**2] # check min dist b/w existing loc and candidates are greater than nb
        nb_candidate = list(set(temp1))
    if ravel:
        return [ravelIdx(x,(nY, nX),True) for x in vCoor]
    else:
        return vCoor

# Demonstration of the vessel_init
if __name__ == "__main__":
    testnY = 21
    testnX = 31
    nVessel = 23
    sep = 10
    print('Creating a random vessel initialisation of ')
    print('Size {0:d} by {1:d}, with {2:d} vessels'.format(testnY, testnX, nVessel))
    print('Degree of separation = {0:d}'.format(sep))
    vIndex = vesselIndex(testnY, testnX, nVessel, sep) 
    print(vIndex)
