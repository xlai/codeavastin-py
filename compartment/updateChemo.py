import __init__
import compartment.const.drug as drug
import compartment.const.grid as grid
import numpy as np
import time as tictoc
import math

from compartment.utilFunc import *
from dolfin import *

def updateChemo(source, sourceNew, sink, drug_p, dt, t, mymesh, sol,ktrans, ktransNew):
    """This is a module update time-stepping solution of three chemotherapeutic drugs.

    Args:
        source, sourceNew(Function): Vessel, source of drugs
        sink (Function): Cell, sink of drugs
        sol (Function): previous solution of G
        t (int): iteration step at which the values are updated
        drug_p (Dictionary): plasma concentration of all drugs
        v (TestFunction): TestFunction of CG1
        g (TrialFunction): TrialFunction of CG1
    """
    update_interval = 30 #30mins of update interval
    inner_time = Constant(0.0)

    # Current solution
    V = FunctionSpace(mymesh, "CG", 1)
    g = TrialFunction(V)
    v = TestFunction(V)
    
    # Previous solution
    g_ = sol
    g_mid = 0.5*(g_ + g)

    # Define drug concentration
    G_p = Constant(0.0)
    currentIndex = int(t/dt)
    
    def f(g):
        return (drug.rG1(ktransNew)*(G_p - g)*sourceNew  + drug.rG1(ktrans)*(G_p - g)*source - drug.phi*g*sink - drug.psi*g) 

    dt = Constant(dt)
    
    F = (g - g_)*v*dx() + dt*inner(drug.D*grad(g_mid), grad(v))*dx() - dt*f(g_mid)*v*dx()
    (a, L) = system(F)

    inner_time.assign(float(dt/2))

    A = assemble(a)
    b = Vector()


    PETScOptions.set("ksp_max_it", 5000)
    PETScOptions.set("ksp_rtol", 1e-8)
    PETScOptions.set("ksp_atol", 1e-8)
    PETScOptions.set("ksp_type", "cg")
    PETScOptions.set("pc_type", "hypre")
    PETScOptions.set("pc_hypre_type", "boomeramg")

    solver = PETScKrylovSolver()
    solver.set_from_options()

    
    while float(inner_time) < (update_interval - float(dt)/2):

#        print "Solving at t = ", float(inner_time)

        # Update G_p to right number (solution of other ODE)
        G_p.assign(0.5*(drug_p[currentIndex] + drug_p[currentIndex+1]))
        # Solve for current solution
        assemble(L, tensor=b)
        solver.solve(A, sol.vector(), b)

        # Assign previous solution
        g_.assign(sol)
        
        # Update time
        inner_time.assign(float(inner_time) + float(dt))
        currentIndex +=1

# Demonstration of the chemo module
if __name__ == "__main__":
    from compartment.const import grid
    from compartment import initialiseVessel
    #set random seed
    np.random.seed(1000)
    
    grid.loadFEM(23,31)

    # Initialise vessels randomly
    vessel = grid.myVessel
    vp = 10
    vessel_init = np.zeros((31 + 1) * (23 + 1))    
    vIndex = initialiseVessel.vesselIndex(21, 31, int(310*230*vp/100/3.1415926/(10**2)), 3, True)
    for i in vIndex:
        vessel_init[i] = 1.0
    vessel.vector()[:] = vessel_init[grid.permSetValue] # initialise mes    

    # Initialise grid. randomly
    myCell = grid.myCell
    n_grid_points = (23+1)*(31+1)
    frac_normal = 0.2
    frac_cancer = 0.5
    n_normal_cells = int(round(n_grid_points*frac_normal))
    n_cells = int(round(n_normal_cells + n_grid_points*frac_cancer))
    rand_non_repeat = np.random.choice(n_grid_points, n_cells, replace=False)
    cell_init = np.zeros(n_grid_points)
    for i in rand_non_repeat[0:n_normal_cells]:
        cell_init[i] = 1
    for i in rand_non_repeat[n_normal_cells:]:
            cell_init[i] = 1
    myCell.vector()[:] = cell_init

    ktrans = 0.001
    ktransNew = 0.001    

    dt = 1
    timer = Timer("Drug PDE solve")
    G1 = grid.G1
    G2 = grid.G2
    
    #initialise drug
    drug_plasma = {}
    drug_plasma['G1'] = drug.CpG1mod(dt_update=dt).tolist()

    file = File('./test/g1.pvd')
    file2 = File('./test/gp.pvd')

    T = 60
    for t in range(0, T+1):
        if t % 30 == 0:
            updateChemo(vessel,grid.myVesselNew, myCell, drug_plasma['G1'], dt, t, grid.mymesh, G1, ktrans, ktransNew)
        # Store current solution
            file << G1
            
    toc = timer.stop()
    print("time to solve = {}".format(toc))
