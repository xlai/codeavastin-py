import compartment.const.oxygen as o2
import compartment.const.grid as grid
import compartment.const.vegf as vegf
import compartment.const.drug as drug
import compartment.const.avastin as av
import numpy as np
import time as tictoc
import math

import initialiseVessel
from utilFunc import *
from dolfin import *


parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True

def updateVA(myVessel, myVesselNew, myCell, vegf_s, drug_p, dt, t, u, ktrans, ktransNew, mymesh):
    """This is a module update steady-state solution of VEGF and Avastin as well as the bound compound, C.

    Args:
        myVessel (Function): Vessel
        myCell (Function): Cell
        vegf_s (Function): subcellular vegf level at every location
        drug_p (Dictionary): plasma concentration of all drugs
        t (int): iteration step at which the values are updated
        VA (Function): previous solution of VEGF, Avastin and C
        ktrans (float): K_trans value extracted from MR
        p,q,s (TestFunction): TestFunction of VEGF, avastin and C respectively
    """
    update_interval = 30 #30mins of update interval
    inner_time = Constant(0.0)
    
    # Current solution
    V=VectorFunctionSpace(mymesh, "CG", 1, dim=3)
    (v1, v2, v3) = TestFunctions(V)
    (u1, u2, u3) = split(u)

    # Previous solution
    u_ = u
    (u1_, u2_, u3_) = split(u_)
    u1_mid = 0.5*(u1 + u1_)
    u2_mid = 0.5*(u2 + u2_)
    u3_mid = 0.5*(u3 + u3_)    

    # Define avastin concentration
    A_p = Constant(0.0)
    currentIndex = int(t/dt)
    
    # Define reaction terms
    def f1(w):
        return (inner(vegf.r(vegf_s),myCell) - vegf.psi * w)
    def f2(w):
        return (av.r(ktrans) * (A_p - w) * myVessel + \
                av.r(ktransNew) * (A_p - w) * myVesselNew - \
                av.psi * w)
    def f3(w):
        return (-vegf.psi*w)
    
    dt = Constant(dt)

    inner_time.assign(float(dt/2))
    
    F = (u1 - u1_)*v1*dx() + dt*vegf.D*inner(grad(u1_mid),grad(v1))*dx() \
        + dt*av.ka*u1_mid*u2_mid*v1*dx() - dt*av.kd*u3_mid*v1*dx() \
        + (u2 - u2_)*v2*dx() + dt*av.D*inner(grad(u2_mid),grad(v2))*dx() \
        + dt*av.ka*u1_mid*u2_mid*v2*dx() - dt*av.kd*u3_mid*v2*dx() \
        + (u3 - u3_)*v3*dx() + dt*vegf.D*inner(grad(u3_mid),grad(v3))*dx() \
        - dt*av.kd*u1_mid*u2_mid*v2*dx() + dt*av.kd*u3_mid*v3*dx() \
        - dt*f1(u1_mid)*v1*dx() - dt*f2(u2_mid)*v2*dx() - dt*f3(u3_mid)*v3*dx()
    file1 = File('./test1/V.pvd')
    file2 = File('./test1/A.pvd')
    file3 = File('./test1/C.pvd') 
   
    while float(inner_time) < (update_interval - float(dt)/2):
        # Update Avastin plasma concentration
        A_p.assign(0.5*(drug_p[currentIndex] + drug_p[currentIndex + 1]))
#        solve(F == 0, u)
        solve(F == 0, u, solver_parameters = {'newton_solver': {'convergence_criterion': 'incremental'}})
        # Assign previous solution
        u_.assign(u)
        # Update time
        inner_time.assign(float(inner_time) + float(dt))
        currentIndex += 1
#        # Store current solution
        (V_copy,A_copy,C_copy) = VA.split(deepcopy = True)            
        file1 << V_copy
        file2 << A_copy
        file3 << C_copy
        
#Testing function
if __name__ == "__main__":
    #set random seed
    np.random.seed(1000)
    
    xx = grid.loadFEM(22,30)

    # Initialise vessels randomly
    vessel = grid.myVessel
    vesselNew = grid.myVesselNew
    vp = 10
    vessel_init = np.zeros((30 + 1) * (22 + 1))    
    vIndex = initialiseVessel.vesselIndex(22,30, int(300*200*vp/100/3.1415926/(10**2)), 3, True)
    for i in vIndex:
        vessel_init[i] = 1.0
        vessel.vector()[:] = vessel_init[grid.permSetValue] # initialise mes    
    # Initialise grid. randomly
    myCell = grid.myCell
    n_grid_points = (22+1)*(30+1)
    frac_normal = 0.2
    frac_cancer = 0.5
    n_normal_cells = int(round(n_grid_points*frac_normal))
    n_cells = int(round(n_normal_cells + n_grid_points*frac_cancer))
    rand_non_repeat = np.random.choice(n_grid_points, n_cells, replace=False)
    cell_init = np.zeros(n_grid_points)
    for i in rand_non_repeat[0:n_normal_cells]:
        cell_init[i] = 1
    for i in rand_non_repeat[n_normal_cells:]:
        cell_init[i] = 1
        myCell.vector()[:] = cell_init

    ktrans = 0.14
    ktransNew = 0.11
    dt = 0.05
    timer = Timer("VA PDE solve")
    VA = grid.VA
    
    #initialise drug
    drug_plasma = {}
#drug_plasma['Avastin'] = drug.CpG3(dt_update=dt)
    drug_plasma['Avastin'] = av.Cp(dt_update=dt)
    (p53,vegf_s)=grid.U.split(True)
    vegf_s.vector()[:]= np.random.rand(31*23)
    
    file0 = File('./test/Ap.pvd')

    T = 30
    for t in range(0, T+1):
        if t % 30 == 0:
            updateVA(vessel, vesselNew, myCell, vegf_s, drug_plasma['Avastin'], dt, t, VA, ktrans, ktransNew, grid.mymesh)

    toc = timer.stop()
    print("time to solve = {}".format(toc))
