
#Function convert coordinate to linear index (default row major)
#Input a tuple of coord (y,x), array size (nY,nX)
#Output
def distSquared(p1,p2):
    return (p1[0]-p2[0])**2 + (p1[1] - p2[1])**2

def neighbours(nY,nX,y,x,nb=1):
    nb_list = []
    for x2 in range(x-nb, x+nb+1):
        for y2 in range(y-nb, y+nb+1):
            if (-1 < x2 < nX and -1 < y2 < nY) and (x != x2 or y != y2):
                nb_list.append((y2,x2))

    return nb_list

def neighboursBorder(nY,nX,y,x,nb=1):
    l1 = neighbours(nY,nX,y,x,nb)
    l2 = neighbours(nY,nX,y,x,nb-1)
    return [x for x in l1 if x not in l2]

def ravelIdx(p,dim, byRow = True):
    if byRow:
        return p[0]*dim[1] + p[1]
    else:
        return p[1]*dim[0] + p[0]
