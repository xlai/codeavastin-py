def count(nx, ny, vp):
    return int(round(nx*ny*100*vp/100/3.1415926/(10**2)))
def permeability(ktrans):
    return ktrans*2*3.1415926*10

def thresholdLow(x):
    return x #constant threshold for now
def thresholdHigh(x):
    return x
def prob_new(x):
    return x #0.002
def prob_del(x):
    return x #0.0001
tau = (12 * 60)/30 #vessel update period = 12hrs, expressed in number of 30mins
