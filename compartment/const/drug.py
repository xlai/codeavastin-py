##
## Drug-related constant values and
## plasma concentration calculation
## according to their pk models

from scipy.integrate import odeint
from dolfin import *
import numpy as np
import math
import compartment.const.vessel as vessel

#Diffusion parameters
D = Constant(9.6*10**(3)) # Diffusion coefficient
## Permeability of all drugs

def rG1(ktrans):
    return Constant(vessel.permeability(ktrans))
def rG2(ktrans):
    return Constant(vessel.permeability(ktrans))
def rG3(ktrans):
    return Constant(vessel.permeability(ktrans))

phi = Constant(0.0) # consumption rate
psi = Constant(0.01) # Decay rate

# Fluorouracil - G1
# Drug plasma conc. calculation according to 2 compartment PK model
# dt_drug: drug injection interval (wk)
# dt_update: algorithm update interval (default 1 min)
# t_total: treatment length (default = 12 wk)
# Weight: patient's weight (default = 58.1 kg)
# Height: patient's height (default = 160 cm)
def G1t(y, t, V0, K0):
    return - V0 * y / (K0 + y)
def CpG1(dt_drug = 3, dt_update = 1, t_total = 12, Weight = 58.1, Height = 160, dose = 600):
    BSA = 0.007184 * (Weight**(0.425)) * (Height**(0.725)) # body surface area m**2
    nt = int(t_total * 7 * 24 * 60 / dt_update) # number of updates
    tG = [ i for i in range(0, nt) if i*dt_update % \
           (dt_drug * 7 * 24 * 60) == 0] # time for drug injection
    ntG = np.diff(tG).tolist() + [nt - tG[-1]]
    DG = [dose * BSA * 10**(3)]*len(tG) #dose of G1 at each injection
    V = 24.0 * (10**(3)) # compartment vol, mL
    Vm = 105.0 /60 #micg mL-1 min-1
    Km = 27.0 #micg mL-1
    sol = [None]*len(tG)
    for i in range(0, len(tG)):
        try:
            sol[i] = odeint(G1t, DG[i]/ V, np.linspace(tG[i],tG[i+1],ntG[i])*dt_update, args=(Vm, Km))
        except IndexError:
            sol[i] = odeint(G1t, DG[i]/ V, np.linspace(tG[i],nt,ntG[i])*dt_update, args=(Vm, Km))
    sol = [x.tolist() for sublist in sol for x in sublist ]
    temp = [x for y in sol for x in y]
    return temp

def CpG1mod(dt_drug = 3, dt_update = 1, t_total = 12, Weight = 58.1, Height = 160, dose = 600):
    BSA = 0.007184 * (Weight**(0.425)) * (Height**(0.725)) # body surface area m**2
    nt = int(t_total * 7 * 24 * 60 / dt_update) # number of updates
    tG = [ i for i in range(0, nt) if i*dt_update % \
           (dt_drug * 7 * 24 * 60) == 0] # time for drug injection
    ntG = np.diff(tG).tolist() + [nt - tG[-1]]
    DG = [dose * BSA * 10**(3)]*len(tG) #dose of G1 at each injection
    V = 24.0 * (10**(3)) # compartment vol, mL
    Vm = 105.0 /60 #micg mL-1 min-1
    Km = 27.0 #micg mL-1
    sol = np.zeros(nt)
    for (index,time_at_injection) in enumerate(tG):
        sol[time_at_injection] = DG[index]/V
        
    time = dt_update
    
    for i in range(1, nt):
        temp_solution = odeint(G1t, sol[i-1],np.array([time-dt_update,time]), args=(Vm, Km))
        sol[i] += float(temp_solution[1])
        time += dt_update
    return sol


# EPIRUBICIN - G2
# Drug plasma conc. calculation according to 3 compartment PK model
# dt_drug: drug injection interval (wk)
# dt_update: algorithm update interval (default 1 min)
# t_total: treatment length (default = 12 wk)
# Weight: patient's weight (default = 58.1 kg)
# Height: patient's height (default = 160 cm)
def CpG2(dt_drug = 3, dt_update = 1, t_total = 12, Weight = 58.1, Height = 160, dose = 100):
    BSA = 0.007184 * (Weight**(0.425)) * (Height**(0.725)) # body surface area m**2
    nt = int(t_total * 7 * 24 * 60 / dt_update) # number of updates
    tG = [ i for i in range(0, nt) if i*dt_update % \
           (dt_drug * 7 * 24 * 60) == 0] # time for drug injection
    DG = [dose * BSA * 10**(3)]*len(tG) #dose of G1 at each injection, mug
    CL = 59. / 60. * (10**(3))  #central clearance, mL/min
    CL2 = 56. / 60. * (10**(3)) #rapid clearance, mL/min
    CL3 = 15. / 60. * (10**(3)) #slow clearance, mL/min
    V1 = 18 * (10**(3))
    V3 = 25 * (10**(3))
    V2 = 1000 * (10**(3)) - V1 - V3

    a0 = CL * CL2 * CL3 / V1 / V2 / V3
    a1 = CL * CL3 / V1 / V3 + CL2 * CL3 / (V2 * V3 + V2 * V1) + \
         CL * CL2 / V1 / V2 + CL3 * CL2 / V3 / V1
    a2 = CL / V1 + CL2 / V1 + CL3 / V1 + CL2 / V2 + CL3 / V3
    p = a1 - a2**(2) / 3. # define update time interval
    q = 2 * a2**(3) / 27. - a1 * a2 / 3. + a0
    r1 = math.sqrt( - (p**(3)) / 27)
    r2 = 2 * (r1**(1./3.));
    phi = math.acos(-q / 2./ r1) / 3.
    alpha = - (math.cos(phi) * r2 - a2 / 3.)
    beta = - (math.cos(phi + 2 * math.pi / 3.) * r2 - a2 / 3.)
    gamma = - (math.cos(phi + 4 * math.pi/ 3.) * r2 - a2 / 3.)
 
    logA = - math.log(V1) + math.log(CL2 / V2 - alpha) + math.log(CL3 / V3 - alpha) - \
           math.log(beta - alpha) - math.log(gamma - alpha)
    logB = - math.log(V1) + math.log( - CL2 / V2 + beta) + math.log(- CL3 / V3 + beta) - \
           math.log(beta - alpha) - math.log(beta - gamma)
    logC = - math.log(V1) + math.log (- CL2 / V2 + gamma) + math.log(CL3 / V3 - gamma) - \
           math.log(beta - gamma) - math.log(gamma - alpha)
#   print([logA,logB,logC])
    pG2 = [[ DG[i]/100000. * (math.exp(5*math.log(10)+(logA - alpha * (ti - tG[i]) * dt_update)) + \
                              math.exp(5*math.log(10)+(logB - beta * (ti - tG[i]) * dt_update)) + \
                              math.exp(5*math.log(10)+(logC - gamma * (ti - tG[i]) * dt_update)) ) \
             if ti>=tG[i] else 0 for ti in range(0,nt)] for i in range(0,len(tG))]
    return [sum(i) for i in zip(*pG2)]

# Cyclophosphamide- G3
# Drug plasma conc. calculation according to 2 compartment PK model
# dt_drug: drug injection interval (wk)
# dt_update: algorithm update interval (default 1 min)
# t_total: treatment length (default = 12 wk)
# Weight: patient's weight (default = 58.1 kg)
# Height: patient's height (default = 160 cm)
def CpG3(dt_drug = 3, dt_update = 1, t_total = 12, Weight = 58.1, Height = 160, dose = 600):
    BSA = 0.007184 * (Weight**(0.425)) * (Height**(0.725)) # body surface area m**2
    nt = int(t_total * 7 * 24 * 60 / dt_update) # number of updates
    tG = [ i for i in range(0, nt) if i*dt_update % \
           (dt_drug * 7 * 24 * 60) == 0] # time for drug injection
    DG = [dose * BSA * 10**(3)]*len(tG) #dose of G3 at each injection, mug
    V = 2430. * (10**(3)) #plasma vol., ml
    CL = 236. * (10**(3)) / 60.  #clearance, ml/min

    pG3 =  [[DG[i] / V * math.exp( - CL / V * (ti - tG[i]) * dt_update) \
             if ti >= tG[i] else 0 for ti in range(0, nt)] \
            for i in range(0,len(tG))]
    return [sum(i) for i in zip(*pG3)]

G1PPC = max(CpG1()) # get peak plasma conc at full dose
G2PPC = max(CpG2())
G3PPC = max(CpG3())

if __name__ == "__main__":
    CpG1 = CpG1()
    CpG2 = CpG2()
    CpG3 = CpG3()
    thefile = open('drugPKTest.txt','w')
    for item in CpG1:
        thefile.write('%5g\t' % item)
    thefile.write('\n') 
    for item in CpG2:
        thefile.write('%5g\t' % item)
    thefile.write('\n')         
    for item in CpG3:
        thefile.write('%5g\t' % item)
    thefile.close()
