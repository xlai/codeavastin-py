##
## Oxygen related constant values
##
from dolfin import *

D = Constant(1.05*10**(5)) # Diffusion coefficient
r = Constant(3.1415926 * 6 * (10**(3))) # Permeability
phi = Constant(15*60) # Consumption by cells
K0 = Constant(20) # tension in blood
K1 = Constant(2.5) # Half-maximal o2 tension


