##
## Avastin related constant values
##
from dolfin import *
import numpy as np
import math
from compartment.const.vessel import permeability

#Diffusion parameters
D = Constant(2.4*10**(3)) # Diffusion coefficient
def r(ktrans): #permeability
    return Constant(permeability(ktrans)/100)
ka = Constant(7.4/10.0) # binding association rate
kd = Constant(3.1*1e-5*60) #disassociation rate
psi = Constant(0.0) # Decay rate

# Avastin plasma conc. calculation according to
# 2-compartment PK model
# dt_drug: drug injection interval (wk)
# dt_update: algorithm update interval (default 30 min)
# t_total: treatment length (default = 12 wk)
def Cp(dt_drug = 3, dt_update = 30, t_total = 12, Weight = 58.1, dose = 15):
    nt = int(t_total*7*24*60/dt_update) # number of updates
    tA = [ i for i in range(0,nt) if i*dt_update % \
           (dt_drug * 7 * 24 * 60) == 0] # time for drug injection
    DA = [dose*Weight*10**(3)]*len(tA) #dose of Avastin at each injection, micg
    CL = 0.207/24/60*10**(3)  #clearance, mL/min
    V = 2.66*10**(3) #volume, mL
    k12 = 0.223/24./60. # 1/min 
    k21 = 0.215/24./60. # 1/min
    Q = k12*V #intercompartmental clearance, mL/min
    V2 = k12/k21*V
    beta = 0.5*(Q/V + Q/V2 + CL/V - math.sqrt((Q/V + Q/V2 + CL/V)**(2) - \
                                              4.0*(Q*CL/V/V2)) )
    alpha = Q*CL/V/V2/beta;
    logA = math.log(alpha - Q/V2) - math.log(alpha - beta) - math.log(V)
    logB = math.log(- beta + Q/V2) - math.log(- beta + alpha) - math.log(V)
    pAvastin = [[DA[i]*(math.exp(logA - alpha*(ti - tA[i])*dt_update) + \
                        math.exp(logB - beta*(ti - tA[i])*dt_update)) \
                 if ti >= tA[i] else 0 for ti in range(0,nt)] for i in range(0,len(tA))]
    return [sum(i) for i in zip(*pAvastin)]

if __name__ == "__main__":
    CpA = Cp()
    thefile = open('AvastinPKTest.txt','w')
    for item in CpA:
        thefile.write('%g\n' % item)
    thefile.close()
