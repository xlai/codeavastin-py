##
## VEGF related constant values
##
from dolfin import *

D = Constant(3.52*10**(3)) # Diffusion coefficient
ka = Constant(7.4/10.0) # binding association rate
kd = Constant(1.86/1000.0) #disassociation rate
psi = Constant(0.01) # Decay rate

def r(VEGFs):
    """
    Calculate production rate of extracelullar VEGF.
    Input:
    VEGFs: FunctionSpace of VEGF expression
    Output:
     FunctionSpace of extracellular VEGF (dolfin object)
    """
    a = 6.661338e-6
    b = -1.098095e-6
#    print(VEGFs.vector().array())
#    print( a * VEGFs.vector().array() + b)
    VEGFcopy = VEGFs.copy(deepcopy=True)
    VEGFcopy.vector()[VEGFcopy.vector() < -b/a] = -b/a
    
    return Constant(a)*VEGFcopy + Constant(b)
