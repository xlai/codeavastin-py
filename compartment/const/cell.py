from dolfin import *

# Specifying cell-cycle related parameters
def Tmin(Tcell): #cell cycle lengths in minutes
    return 24 * 60 * Tcell
Kphi = 1.4 # in mmHg
# p53 equation parameters
def k7(scale = 1):
    return 0.002*scale
#k7 = Constant(0.002*6) # patient2 specific
k77 = 0.01
Kp53 = 0.01
# VEGF equation parameters
def k8(scale = 1):
    return 0.002*scale
k88 = 0.01
J5 = 0.04
Kvegf = 0.01
def k888(scale=1):
    return -0.002/scale

