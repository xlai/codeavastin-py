# Add the project's root directory to the path.
# This allows modules to be able to run from the directories where they are 
# localed, in addition to the root directoy.
import sys
import os.path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 
            os.path.join(os.pardir, os.pardir))))
