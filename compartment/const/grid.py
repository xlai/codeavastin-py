from dolfin import *

# Specify computational domain
def mesh(ny, nx):
    dn = 10 # cell distance in micro-metre
    x1 = nx*dn
    y1 = ny*dn
    return RectangleMesh(Point(0,0),Point(x1,y1), nx,ny)

def CG(mesh):
    return FunctionSpace(mesh, "CG", 1) # continuous mesh
def CG2(mesh):
    return VectorFunctionSpace(mesh, "CG", 1, dim=2) # coupled continuous mesh for p53 - VEGF_s
def CG3(mesh):
    return VectorFunctionSpace(mesh, "CG", 1, dim=3) # coupled continuous mesh for vegf - avastin - VA compound
    
# Define characteristic function
def loadFEM(ny, nx):
    global mymesh, myVessel, myVesselNew, myCell, TP53, VEGFs, K, v, VA, p, q, s, \
        G1, G2, G3, myCellType, permSetValue, permGetValue, g
    mymesh = mesh(ny, nx)
    CG1space = CG(mymesh)
#   CG2space = CG2(mymesh)
    CG3space = CG3(mymesh)
    myVessel = Function(CG1space) #vessel location
    myVesselNew = Function(CG1space)
    myCell = Function(CG1space) #cell location
    TP53 = Function(CG1space)
    VEGFs = Function(CG1space)
#    U = Function(CG2space) # subcellular p53 + VEGF expression
    K = Function(CG1space) #oxygen
    v = TestFunction(CG1space) # test function
    VA = Function(CG3(mymesh)) #VEGF and avastin
#    (w, b) = TestFunctions(CG2space) #test function for p53-VEGF
    (p, q, s) = TestFunctions(CG3space) #test function for VA
    G1 = Function(CG1space) #Chemo 1
    G2 = Function(CG1space) #Chemo 2
    G3 = Function(CG1space) #Chemo 3
    myCellType = Function(CG1space) # cell type
    g = TrialFunction(CG1space)
    permGetValue=vertex_to_dof_map(CG1space) #perm array for getting value at vertex
    permSetValue=dof_to_vertex_map(CG1space) #perm array for setting value at vertex
    
#    return {'mymesh':mymesh, 'myVessel':myVessel, 'myCell':myCell,
#            'U':U, 'K':K, 'v':v, 'VA':VA, 'w':w, 'b':b, 'p':p, 'q':q, 's':s,
#            'G1':G1, 'G2':G2, 'G3':G3, 'myCellType':myCellType}
