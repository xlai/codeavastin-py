import compartment.const.oxygen as o2
import compartment.const.cell as cell
import compartment.const.vegf as vegf
import compartment.const.avastin as av
import numpy as np
import time as tictoc
import math

from compartment.initialiseVessel import *
from compartment.utilFunc import *
from dolfin import *

set_log_level(30)

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True

def updateVASteady(myVessel, myVesselNew, myCell, vegf_s, drug_p, dt, t, VA, ktrans, ktransNew, p, q, s):
    """This is a module update steady-state solution of VEGF and Avastin as well as the bund compound, C.

    Args:
        myVessel (Function): Vessel
        myVessel (Function): New vessel
        myCell (Function): Cell
        vegf_s (Function): subcellular vegf level at every location
        drug_p (Dictionary): plasma concentration of all drugs
        t (int): iteration step at which the values are updated
        VA (Function): previous solution of VEGF, Avastin and C
        ktrans, ktransNew (float): K_trans value extracted from MR
        p,q,s (TestFunction): TestFunction of VEGF, avastin and C respectively
    """
    g = Constant(0.0)
    currentIndex = int(t/dt)
    VA.vector()[:]=0.    
    (V, A_e, C) = split(VA)            
    F1 = - inner(vegf.D * grad(V), grad(p)) * dx + \
         (inner(vegf.r(vegf_s), myCell) - av.ka * A_e * V  + av.kd * C - vegf.psi*V) * p * dx - \
         inner(av.D * grad(A_e), grad(q)) * dx + \
         (av.r(ktransNew) * (Constant(drug_p[currentIndex]) - A_e) * myVesselNew + \
         av.r(ktrans) * (Constant(drug_p[currentIndex]) - A_e) * myVessel - av.ka * A_e * V + av.kd * C - av.psi*A_e) * q * dx -\
         inner(vegf.D * grad(C), grad(s)) * dx + \
         (av.ka * V * A_e - av.kd * C - vegf.psi*C) * s * dx
    solve(F1 == 0, VA, solver_parameters = {'newton_solver': {'convergence_criterion': 'incremental'}})

#Testing function
if __name__ == "__main__":
    # Define variational formulation for diffusion of VEGF (V) and avastin A:
    VA = Function(cell.CG3)
    (w, b
     , d) = TestFunctions(cell.CG3)
    #initialise vessels
    vessel = Function(cell.DG)
    nVessel = 2
#    vCoord = vesselIndex(20,30,nVessel,3) 
#    vIndex = [ravelIdx(x,(20,30),True) for x in vCoord]
#    temp1 = [int(2*x) for x in vIndex] + [int(2*x-1) for x in vIndex]
#    print vIndex
#    for i in temp1:
#        vessel.vector()[i] = 1
    vValues = np.random.randint(0, cell.DG.dim(),nVessel)
    for i in vValues:
        vessel.vector()[i] = 1
    #initialise cells
    myCell = Function(cell.DG)    
    nCell = 1000
    cValues = np.random.randint(0, cell.DG.dim(),nCell)
    for i in cValues:
        myCell.vector()[i] = 1
    vegfFile = File('./vegfFile.pvd')
    avastinFile = File('./avastinFile.pvd')

    timer = Timer("vegf-avastin coupled PDE solve")
    t_update = range(0,10)
    for t in t_update: 
        vaDiffusionSteady(vessel, myCell, t)
        (V,A_e) = VA.split(deepcopy = True)
        vegfFile << V
        avastinFile << A_e
    toc = timer.stop()
    print("time to solve = ".format(toc))
