import random
import copy
from compartment.utilFunc import *

class Cell(object):
    def __init__(self, chance, o2, Tcell, TcellSD):
        self.reset(o2)
        if random.random() <= chance:
            self.prevState = 1
            self.cycle = random.random()
            self.Tcell = random.gauss(Tcell,TcellSD)
            self.state = 1
            self.lived = 1
            self.type = 1            
    def reset(self, o2):
        self.prevState = None
        self.cycle = None
        self.Tcell = None        
        self.state = None
        self.lived = None
        self.o2 = o2
        self.type = None
    
    def getState(self):
        return self.state
    
    ## Subclasses will override this method in order to 
    ## update the appearance of the shape that represents
    ## a cell.
    def setOxygen(self,o2):
        self.o2 = o2
    def getOxygen(self):
        return self.o2
    def setCellCycle(self,cycle):
        self.cycle = cycle
    def getCellCycle(self):
        return self.cycle
    def getType(self):
        return self.type
    def copyType(self, oldObj):
        self.type = oldObj.type
    def setState(self, state):
        self.state = state
        if state == 1:
            self.lived += 1
    def getPrevState(self):
        return self.prevState
    def setPrevState(self, state):
        self.prevState = state
    def copyState(self):
        self.prevState = self.state
        
    def getNumLived(self):
        return self.lived
    
class CancerCell(Cell):
    def __init__(self, chance, o2, Tcell, TcellSD):
        Cell.__init__(self, chance, o2, Tcell, TcellSD)
        if random.random() <= chance:
            self.type = 2
            #### removed feature ####
            self.slept = 0
#### removed feature ####            
    def getSleep(self):
        return self.slept  
    def setSleep(self):
        KEnter = 8.9
        KLeave = 9.8        
        if self.o2 <= KEnter:
            self.slept += 1
        elif self.o2 >= KLeave:
            self.slept = 0
class Automata(object):
    def __init__(self, numcols, numrows, chanceOfLife, oxygen, Tcell, TcellSD, chanceOfCancer):
        self.rows = numrows
        self.cols = numcols
        self.numcells = numrows * numcols
        
        self.cells = []
        # Make a (row) list.
        for i in range(numrows):
            row = []
            # Make a list of (column) cells for each row
            for j in range(numcols):
                try:
                    cell = self.getACell(chanceOfLife[i*numcols + j], oxygen[i*numcols + j], Tcell, TcellSD, chanceOfCancer[i*numcols+j])
                except(TypeError):
                    cell = self.getACell(chanceOfLife, oxygen[i*numcols + j], Tcell, TcellSD,  chanceOfCancer)
                row.append(cell)
            self.cells.append(row)
                
    ## Subclasses will override this method so that
    ## instances of their subclass of Cell will be used by
    ## the automata.
    def getACell(self, chanceOfLife, o2, Tcell, TcellSD, chanceOfCancer = 0):
        if random.random() <= chanceOfCancer:
            return CancerCell(chanceOfLife, o2, Tcell, TcellSD)
        else:
            return Cell(chanceOfLife, o2, Tcell, TcellSD)
    ## Subclasses will override this method in order to 
    ## position the shapes that represent the cells.
        
    def getAllCells(self):
        return self.cells
    def countLiving(self):
        numLiving = 0.0
        for i in range(self.rows):
            for j in range(self.cols):
                numLiving += self.cells[i][j].getState()
        return numLiving
########## removed feature ####
    def checkSleep(self):
        for i in range(self.rows):
            for j in range(self.cols):
                try:
                    self.cells[i][j].setSleep()
                except(AttributeError):
                    continue
    
    def nextGeneration(self):
        # Move to the "next" generation
        for i in range(self.rows):
            for j in range(self.cols):
                self.cells[i][j].copyState()
                
        for i in range(self.rows):
            for j in range(self.cols):
                currCell = self.cells[i][j]
                if currCell.getType() == 2 and currCell.getCellCycle() >= 1.:
#                    print('I\'m checking proliferation!')
                    nbList = neighbours(self.rows,self.cols,i,j)
                    # Query the state of the neighborhood and update
                    #prevState = self.cells[i][j].getPrevState()
                    Automata.applyRules(self, currCell, nbList)

    ## Subclasses can override this method in order to apply
    ## their own custom rules of life.
    def applyRules(self, cell, nbList):
        nbStates = [self.cells[i[0]][i[1]].getPrevState() for i in nbList]
        if None not in nbStates: # neighbour full, reset cell cycle
            cell.setCellCycle(0)
            cell.lived=1
        else:
            nbO2 = [self.cells[i[0]][i[1]].getOxygen() * (1 - int(nbStates[idx] or 0)) for (idx,i) in enumerate(nbList)]
            maxO2 = max(nbO2)
            newIdx = random.choice([i for (i,x) in enumerate(nbO2) if x == maxO2])
            cell.setCellCycle(0)
            cell.lived=1
            self.cells[nbList[newIdx][0]][nbList[newIdx][1]] = copy.deepcopy(cell)
            self.cells[nbList[newIdx][0]][nbList[newIdx][1]].setOxygen(maxO2)
            self.cells[nbList[newIdx][0]][nbList[newIdx][1]].Tcell = random.gauss(cell.Tcell,1)
