import compartment.const.drug as drug
import compartment.const.cell as cell
import numpy as np
import time as tictoc
import math

from initialiseVessel import *
from utilFunc import *
from dolfin import *

def updateChemoSteady(source, sink, drug_p, t, G1, G2, G3, ktrans, v):
    """This is a module update steady-state solution of three chemotherapeutic drugs.

    Args:
        source (Function): Vessel, source of drugs
        sink (Function): Cell, sink of drugs
        G1 (Function): previous solution of G1
        G2 (Function): previous solution of G2
        G3 (Function): previous solution of G3
        t (int): iteration step at which the values are updated
        drug_p (Dictionary): plasma concentration of all drugs
        v (TestFunction): TestFunction of CG1
    """
    # Define variational formulation of oxygen concentration G1    
    FG1 = - inner(drug.D * grad(G1), grad(v)) * dx + \
          drug.rG1(ktrans) * (Constant(drug_p['G1'][t]) - G1) * source * v * dx - \
          drug.phi * G1 * sink * v * dx - drug.psi * G1 * v * dx
    solve(FG1 == 0, G1, solver_parameters = {'newton_solver': {'convergence_criterion': 'incremental'}})
    # Define variational formulation of oxygen concentration G2
    FG2 = - inner(drug.D * grad(G2), grad(v)) * dx + \
          drug.rG2(ktrans) * (Constant(drug_p['G2'][t]) - G2) * source * v * dx - \
          drug.phi * G2 * sink * v * dx - drug.psi * G2 * v * dx
    solve(FG2 == 0, G2, solver_parameters = {'newton_solver': {'convergence_criterion': 'incremental'}})
    # Define variational formulation of oxygen concentration G3    
    FG3 = - inner(drug.D * grad(G3), grad(v)) * dx + \
          drug.rG3(ktrans) * (Constant(drug_p['G3'][t]) - G3) * source * v * dx - \
          drug.phi * G3 * sink * v * dx - drug.psi * G3 * v * dx
    solve(FG3 == 0, G3, solver_parameters = {'newton_solver': {'convergence_criterion': 'incremental'}})

# Demonstration of the chemo module
if __name__ == "__main__":
    G1 = Function(cell.CG)
    G2 = Function(cell.CG)
    G3 = Function(cell.CG)    
    v = TestFunction(cell.CG)

    # Initialise vessels randomly
    vessel = Function(cell.DG)
    nVessel = 10
    vValues = np.random.randint(0, cell.DG.dim(),nVessel)
    for i in vValues:
        vessel.vector()[i] = 1

    # Initialise cells randomly
    myCell = Function(cell.DG)    
    nCell = 400
    cValues = np.random.randint(0, cell.DG.dim(),nCell)
    for i in cValues:
        myCell.vector()[i] = 1

    # Saving results to file    
    G1File = File('./G1File.pvd')
    G2File = File('./G2File.pvd')
    G3File = File('./G3File.pvd') 

    timer = Timer("Drug PDE solve")
    for t in range(0, 10):
        chemoDiffusionSteady(vessel, myCell,t)
        G1File << G1
        G2File << G2
        G3File << G3
    toc = timer.stop()
    print("time to solve = ".format(toc))
