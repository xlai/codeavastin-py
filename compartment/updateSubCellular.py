import __init__
import math
import compartment.const.cell as cell
import compartment.const.grid as grid
import numpy as np
from scipy.integrate import solve_ivp
from dolfin import *

def updateSubCellular(outer_dt, hypoxia_scale1, hypoxia_scale2, hypoxia_scale3,  K, TP53, VEGFs):
    # Define right hand side of the ODE(s)
    local_p53 = TP53.vector()[:]
    local_VEGFs = VEGFs.vector()[:]
    local_o2 = K.vector()[:]
    B = cell.k77/(cell.Kp53/local_o2 + 1.0)
    p53_at_end_time = cell.k7(hypoxia_scale1)/B + \
        (local_p53 - cell.k7(hypoxia_scale1)/B) * np.exp(-B*outer_dt)    
    fun=lambda t, y: rhs_p53_vegf_ode(cell, hypoxia_scale2, hypoxia_scale3, p53_at_end_time, y, local_o2)
    sol = solve_ivp(fun, (0, outer_dt),
                    local_VEGFs)
    vegf_at_end_time = [x[-1] for x in sol.y]
    TP53.vector()[:] = p53_at_end_time
    VEGFs.vector()[:] = vegf_at_end_time

def rhs_p53_vegf_ode(cell, hypoxia_scale2, hypoxia_scale3, p53, VEGFs, o2):
    """Right hand side of the couple ODE's for the P53 and VEGF.
    
    Coupled ODE's: 
        d p53/dt = k_7 - k_77*o2/(k_p53 + o2)*p53 
        d vegf/dt = k_8 + k_888*p53*vegf/(j_5 + vegf) 
            - k_88*o2/(k_vegf + o2)*vegf
    Args:
        p53_vegf (list): [p53, vegf]
    """
    return cell.k8(hypoxia_scale3) + \
        cell.k888(hypoxia_scale2) * p53 * VEGFs / (cell.J5 + VEGFs) - \
        cell.k88 * o2 / (cell.Kvegf + o2) * VEGFs

    
if __name__ == '__main__':
    # Function space and functions
    mymesh = grid.mesh(20,30)
    V = FunctionSpace(mymesh, "CG", 1)
    TP53 = Function(V)
    VEGFs = Function(V)    
    K = Function(grid.CG(mymesh))
    K.vector()[:] = np.ones(grid.CG(mymesh).dim())*3.8
#    pV.vector()[:] = np.concatenate((np.ones(cell.CG.dim())*1.088, np.ones(cell.CG.dim())*0.15148), axis = 0)
    dt = 30. #min
    t_total = 60
    x_save = []
    for i in range(0,t_total, int(dt)):
        updateSubCellular(dt, 1,1,1, K, TP53, VEGFs)
              
