import compartment.const.oxygen as o2
import compartment.const.grid as grid
import compartment.const.vessel as vessel
import numpy as np

def updateVessel(vegf_low, vegf_high, prob_birth, prob_death, permVessel, V, myVessel, myVesselNew):
    Kv = V.vector()[grid.permGetValue] #get vegf value
    xi = myVessel.vector()[grid.permGetValue]
    xiNew = myVesselNew.vector()[grid.permGetValue]
    
    nbirth = np.random.poisson(vessel.prob_new(prob_birth)*vessel.tau)
    birthVEGF = np.all([Kv > vessel.thresholdLow(vegf_low), Kv < vessel.thresholdHigh(vegf_high)],axis=0)*(1 - xi - xiNew)*Kv
    if sum(birthVEGF) > 0:
        birthVEGFWeighted = birthVEGF / sum(birthVEGF)
    else:
        birthVEGFWeighted = birthVEGF
        
    deathVEGF = np.logical_or(Kv > vessel.thresholdHigh(vegf_high),Kv < vessel.thresholdLow(vegf_low)) * ( 1 - permVessel)*(xi+xiNew)
    deathIdx = np.nonzero(deathVEGF * np.random.binomial(1, vessel.prob_del(prob_death), len(Kv)))
    try:
        birthIdx = np.random.choice(range(len(Kv)), size=nbirth, replace=False, p = birthVEGFWeighted)
    except ValueError:
        birthIdx = np.nonzero(birthVEGF != 0)
    #update vessel index according to birth/death
    xi[deathIdx] = 0
    xiNew[deathIdx] = 0
    xiNew[birthIdx] = 1
    myVessel.vector()[:] = xi[grid.permSetValue]
    myVesselNew.vector()[:] = xiNew[grid.permSetValue]

if __name__ == '__main__':
    permvessel = np.zeros((cell.nx + 1) * (cell.ny + 1))
    vCoor = vesselIndex(cell.ny, cell.nx, vessel.nVessel, vessel.sep)
    vIndex = [ravelIdx(x,(cell.ny + 1, cell.nx + 1),True) for x in vCoor]
    for i in vIndex:
        permvessel[i] = 1.0
    myVessel.vector()[:] = permvessel[grid.permSetValue]
    permvessel = np.zeros((cell.nx + 1) * (cell.ny + 1))
    VA = Function(cell.CG3) #VEGF and avastin
    (p, q, s) = TestFunctions(cell.CG3) #test function for VA
    updateVASteady(myVessel, myCell, vegf_s, ti, VA, p, q, s)
    (V, A_e, C) = VA.split(deepcopy = True)
