import numpy as np
import math
import time as tictoc

from initialiseVessel import *
from utilFunc import *
from dolfin import *

set_log_level(WARNING)

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
# Parameter inputs -
# r_k permeability
# C_0 plasma concentration of CA at different time
# nVessel number of vessels
# sd degrees of separation between vessels
def CA_diffusion(r_c,C_0,nVessel,sd):    
    # Specify the computational domain
    dn = 10 # cell distance in micro-metre
    N = 22
    M = 30
    mesh = RectangleMesh(Point(0,0),Point(M*dn,N*dn),M,N)
    #mesh = UnitSquareMesh(N, M)
    # Specify time
    nT = len(C_0)      # get number of time interval
    T = 13.0/60.0      # time interval
    num_steps = 10     # number of time steps
    dt = T / num_steps # time step size
    
    # Define the function space, continuous piecewise linears
    CG = FunctionSpace(mesh, "Lagrange", 1)
    
    # Define the unknown function and the test function for the
    # variational formulation
    C = Function(CG)
    v = TestFunction(CG) # Contrast Agent(CA) conc. in tissue
    
    # Material parameters
    # Diffusion constant
    D_c = Constant(3.09*10**(4))
    
    # Permeability
    r_c = Constant(r_c)
    
    # Define characteristic function for vessels
    Xi = Function(CG)
    vIndex = vesselIndex(N + 1, M + 1, nVessel, sd, True)
    print vIndex
    vessel_init = np.zeros((M + 1) * (N + 1))        
    for i in vIndex:
        vessel_init[i] = 1.0
    Xi.vector()[:] = vessel_init[vertex_to_dof_map(CG)] # initialise mesh value according to DOF map        
    temptemp = Xi.vector()[:]

    thefile = open('vessel.txt','w')
    for item in temptemp:
        thefile.write('%g\n' % item)
    thefile.close()
    # Create VTK file for saving solution
    vtkfile = File('CA_valid/solution.pvd')

    # Time-stepping
    t = 0
    
    # Set initial condition
    C.vector()[:] = 0.0
    C_n = Function(CG)
    C_n.vector()[:] = 0.0

    g = Constant(0.0)
    timer = Timer("CA PDE solve")

    # Save average concentration
    C_avg = []
    for i in range(len(C_0)):
        # Define variational formulation of CA concentration C
        F = - inner(C,v)*dx - dt*inner(D_c*grad(C), grad(v))*dx \
            + (C_n + dt*r_c*(C_0[i] - C)*Xi)*v*dx + g*v*ds

        for n in range(num_steps):
            t += dt
            solve(F == 0, C)
            # Save to file and plot solution
#            if (i == 10):

            # Update previous solution
            C_n.assign(C)
        vtkfile << (C,t)            
        C_avg.append(np.mean(C.vector()[:]))
    toc = timer.stop()
    print("time to solve = ", toc)
    ttemp = C.vector()[:];
    thef = open('Cfinal.txt','w')
    for item in ttemp:
        thef.write('%.10f\n' % item)
    thef.close()
    return C_avg
#read patient and MR-specific AIF value
AIF_file = open('../../MRI_data/avastin0048/AIF/AIF_MR0p.txt')
AIF = [line.strip() for line in AIF_file]
AIF_file.close()
AIF1 = [float(i.replace(',','.')) for i in AIF] #replace comma with point and convert to float
r1 = 5.2     # relaxivity value
he = 0.41         # haematocrit value
C_plasma = [i*(i>0.1)/r1*604.71/1000 for i in AIF1]
print(C_plasma)

vp = 20.4661
nV = int(round(230.*309.*vp/100/3.1415926/(10**2)))
ktrans = 0.2145*2*3.1415926*10
print(ktrans)
print(nV)
#C1 = CA_diffusion(ktrans,C_plasma,nV,1)
#print(C1)
#rCI_file = open('./avastin0046_rCI.txt');
#rCI0 = [line.strip() for line in rCI_file]
#rCI_file.close()
#temp = list()
#temp.append([rCI0[i].split(',') for i in range(0,len(rCI0))])
#temp = temp[0]
#rCI = [[float(j) for j in temp[i]] for i  in range(0,len(temp))]

for sep in range(2,7):
    Ci  = CA_diffusion(ktrans,C_plasma,nV,sep)
    print(Ci)
#    C_error = sum(x < rCI[0][i] or  x > rCI[2][i] for i,x in enumerate(Ci))
#    print(C_error)

