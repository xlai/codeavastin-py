n#!/bin/bash
( set -o posix ; set ) >/tmp/variables.before
f_input='avastin0012'
size=(22 30)
ktrans=0.01
ktransNew=0.14
vp=11.97
Tcell=14.69
v_sep=4
chemo_theta='{"G1":6000,"G2":6000,"G3":6000}'
drug_schedule='{"G1":3,"G2":3,"G3":3,"avastin":3}'
drug_dose='{"G1":600,"G2":100,"G3":600,"avastin":0}'
weeks=12
seed=1234
foo () {
    local run=$1
    local idx=$2
    opath=./${f_input}_${seed}_$(date +%Y%m%d_%H%M%S)_${run}
    mkdir -p $opath
    ( set -o posix ; set ) >/tmp/variables.after
    comm -13 <(sort < /tmp/variables.before) <(sort < /tmp/variables.after) > $opath/variables.out
    time python ../main.py ${f_input} ${size[0]} ${size[1]} ${Tcell} -v=${vp[${idx}]} -s=${v_sep[${idx}]} -z=${chemo_theta} -n=${weeks} -o ${opath} --schedule=${drug_schedule} --ktrans=${ktrans} --ktransNew=${ktransNew} --dose=${drug_dose} --seed=${seed} --no-output >> $opath/file.out <<EOF
 0.4
 0.3
EOF
}

for j in 0
do
    for i in {1}
    do
	echo ${size[0]} ${size[1]}	
	foo $i $j &
    done
    wait
    echo "1 set finished"
done

rm /tmp/variables.before /tmp/variables.after

wait
echo "All processes done!"
