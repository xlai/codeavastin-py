#!/bin/bash

f_input='avastin0018'
ktrans=0.13
vp=11.97
Tcell=14.69
v_sep=4
chemo_theta='{"G1":10000,"G2":10000,"G3":10000}'
drug_schedule='{"G1":3,"G2":3,"G3":3,"avastin":3}'
drug_dose='{"G1":600,"G2":100,"G3":600,"avastin":0}'
weeks=12

foo () {
    local run=$1
    local idx=$2
    opath=./${f_input}_${ktrans}_${vp[${idx}]}_$(date +%Y%m%d_%H%M%S)_${run}
    mkdir -p $opath
    python main.py ${f_input} ${Tcell} -v=${vp[${idx}]} -s=${v_sep[${idx}]} -z=${chemo_theta} -n=${weeks} -o ${opath} --schedule=${drug_schedule} --dose=${drug_dose} --vegf_high=${vegf_high} --no-output --nice > $opath/file.out
}
j=0
for vegf_high in '5e-6' '1e-5' '5e-5' '1e-4'
do
    for i in {1..8}
    do
	foo $i $j $vegf_high&
    done
    wait
    echo "1 set finished"
done


wait
echo "All processes done!"
