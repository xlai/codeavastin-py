#!/bin/bash

f_input='avastin0018'
vp=(11.97 20)
Tcell=14.69
v_sep=(4 3)
chemo_theta=('{"G1":2000,"G2":2000,"G3":2000}' '{"G1":4000,"G2":4000,"G3":4000}' '{"G1":6000,"G2":6000,"G3":6000}' '{"G1":8000,"G2":8000,"G3":8000}' '{"G1":10000,"G2":10000,"G3":10000}' '{"G1":12000,"G2":12000,"G3":12000}')
drug_schedule='{"G1":3,"G2":3,"G3":3,"avastin":3}'
drug_dose='{"G1":600,"G2":100,"G3":600,"avastin":15}'
weeks=12
h_scaling=1
ktrans=0.13

foo () {
    local run=$1
    local id=$2
    local idx=0
    opath=./${f_input}_${ktrans}_${vp[${idx}]}_$(date +%Y%m%d_%H%M%S)_${run}
    mkdir -p $opath    
    python main.py ${f_input} ${Tcell} -v=${vp[${idx}]} -s=${v_sep[${idx}]} -z=${chemo_theta[${id}]} -n=${weeks} -o ${opath} --schedule=${drug_schedule} --dose=${drug_dose} -x=${h_scaling} --vegf_high=${vegf_high} --no-output --nice > $opath/file.out
}

for vegf_high in '1e-4'
do
    for j in {0..5}
    do
	for i in {1..8}
	do
	    foo $i $j $vegf_high&
	done
	wait
	echo "1 set finished"
    done
done


wait
echo "All processes done!"
